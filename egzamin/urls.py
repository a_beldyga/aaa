from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'egzamin.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^main/', 'app.views.main', name='main'),
    url(r'^logout/$','app.views.user_logout', name='logout'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^login/$', 'app.views.authentication', name='login'),
    url(r'^canvas/$', 'app.views.canvas', name='canvas'),
    url(r'^lista/$', 'app.views.lista', name='lista'),
    url(r'^lista/zip/courses/(?P<kurs>[0-9]{6})', 'app.views.zip', name='zip'), #wsgi dodac zeby dzialalo

)
