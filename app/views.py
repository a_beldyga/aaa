# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.shortcuts import render_to_response, redirect
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from form import *
from models import *
import tasks
from django.http import HttpResponse
import memcache

# Create your views here.

mc = memcache.Client(['194.29.175.241:11211'])


def main(request):
    # newsy= Dane.objects.all().order_by('-pk')
    # users = User.objects.all()
    # return render(request, 'base.html', {'newsy': newsy, 'users': users})

    return render(request, 'base.html')


@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return redirect(reverse('main'))

def authentication(request):
    if request.method == 'GET':
        c = {}
        c.update(csrf(request))
        return render_to_response('login.html', c)
    if request.method == 'POST':
        form=AuthForm(request.POST)
        if form.is_valid():
            # the password verified for the user
            user = form.login(request)
            if user.is_active:
                print("User is valid, active and authenticated")
                login(request, user)
                return redirect(reverse('main'))
            else:
                print("The password is valid, but the account has been disabled!")
        else:
            # the authentication system was unable to verify the username and password
            print("The username and password were incorrect.")
            return render_to_response('login.html', {'form': form}, context_instance=RequestContext(request))


def canvas(request):


    if request.method == 'GET':
            c = {}
            c.update(csrf(request))
            return render_to_response('canvas.html', c)
    if request.method == 'POST':
            form = CanvasForm(request.POST) # A form bound to the POST data
            user=request.user
            login = form.data['login']
            password = form.data['password']

            instance = UserProfile.objects.get(id=request.user.id)

            if(UserProfile.objects.filter(id=instance.user.id).exists()):
                instance.delete()


            r = UserProfile(user=user, login = login, password = password)
            r.save()

            print user.username
            print login
            print password
            return redirect(reverse('main'))


    else:
        return redirect(reverse('blog'))

def lista(request):

    user = UserProfile.objects.get(id = request.user.id)
    log = user.login
    pas = user.password


    r = tasks.get_courses.delay(log, pas)

    return render(request, 'lista.html', {'kursy': r['kursy'], 'slownik': r['slownik']})

def zip(request, kurs):
    user = UserProfile.objects.get(id = request.user.id)
    log = user.login
    pas = user.password

    aktywnosc = mc.get('aktywnosc_%s' % str(kurs))
    prace_domowe = mc.get('prace_domowe_%s' % str(kurs))
    egzamin = mc.get('egzamin_%s' % str(kurs))
    laboratoria = mc.get('laboratoria_%s' % str(kurs))

    if not aktywnosc:
        r = tasks.get_grades.delay(log, pas, kurs)
        aktywnosc = r['aktywnosc']
        mc.set('aktywnosc_%s' % str(kurs), aktywnosc, 300)
        prace_domowe = r['prace_domowe']
        mc.set('prace_domowe_%s' % str(kurs), prace_domowe, 300)
        egzamin = r['egzamin']
        mc.set('egzamin_%s' % str(kurs), egzamin, 300)
        laboratoria = r['laboratoria']
        mc.set('laboratoria_%s' % str(kurs), laboratoria, 300)


    l= tasks.create_zip.delay(aktywnosc, prace_domowe, egzamin, laboratoria)


    response = HttpResponse(l, content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=myfile.zip'
    return response



